/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problem;

import ensemble.weighted.EvaluateWeightedEoC;


import java.util.ArrayList;
import java.util.List;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;
import weka.classifiers.Evaluation;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class DEWeightedEOC extends Problem {

    protected int kFolds;
    protected String cvModelPath;
    protected Instances[] trnCvFold;
    protected Instances[] tstCvFold;
    protected Instances trnData;
    protected Evaluation eval;
    protected String[] clsfNames;
    protected boolean isDebug=false;
    
    /**
     * Constructor Create a default Instance of problem Weighted Ensemble of
     * Classifiers.
     *
     * @param solutionType The Solution type must "Real"
     * @throws JMException
     */
    public DEWeightedEOC(String solutionType, Integer numberOfVariables) throws ClassNotFoundException {
        numberOfVariables_ = numberOfVariables.intValue();
        numberOfObjectives_ = 1;
        numberOfConstraints_ = 0;
        problemName_ = "DEWeightedEOC";

        upperLimit_ = new double[numberOfVariables_];
        lowerLimit_ = new double[numberOfVariables_];

        for (int i = 0; i < numberOfVariables_; i++) {
            lowerLimit_[i] = -1.0;
            upperLimit_[i] = 1.0;
        } // for

        if (solutionType.compareTo("Real") == 0) {
            solutionType_ = new RealSolutionType(this);
        } else {
            System.out.println("Error: solution type " + solutionType + " invalid");
            System.exit(-1);
        }
    }
    
    public DEWeightedEOC(String solutionType, Integer numberOfVariables, int fold, String cvMpath, Instances[] trn, Instances[] tst, String[] clsf, Instances dataTrn) {
        this.cvModelPath = cvMpath;
        this.trnCvFold = trn;
        this.tstCvFold = tst;
        this.clsfNames = clsf;
        this.trnData = dataTrn;
        this.kFolds = fold;
        
        numberOfVariables_ = numberOfVariables.intValue();
        numberOfObjectives_ = 1;
        numberOfConstraints_ = 0;
        problemName_ = "DE-EoC";

        upperLimit_ = new double[numberOfVariables_];
        lowerLimit_ = new double[numberOfVariables_];

        for (int i = 0; i < numberOfVariables_; i++) {
            lowerLimit_[i] = 0.0;
            upperLimit_[i] = 1.0;
        } // for

        if (solutionType.compareTo("Real") == 0) {
            solutionType_ = new RealSolutionType(this);
        } else {
            System.out.println("Error: solution type " + solutionType + " invalid");
            System.exit(-1);
        }
        
    }    

    // Normalize array elements so that total weight is equal to 1.0
    public static void normalize(double[] arr) {
        double sum = 0.0;
        int nElem = arr.length;
        for (int i = 0; i < nElem; i++) {
            sum += arr[i];
        }

        for (int i = 0; i < nElem; i++) {
            arr[i] = arr[i] / sum;
        }
    }
    
    @Override
    public void evaluate(Solution solution) throws JMException {
        XReal x = new XReal(solution);
// Get the Ensemble Combination
        List<Double> weightCmb = new ArrayList<Double>();
        double[] soln = new double[numberOfVariables_];
        double xi; // auxiliar variables
        
        for (int var = 0; var < numberOfVariables_ - 1; var++) {
            xi = x.getValue(var);
            soln[var]=xi;
        } // for
        normalize(soln);
        
        for(int idx=0; idx<soln.length; idx++)
            weightCmb.add(soln[idx]);
        
        // Objective value calculation for Objective Function 1: MCC
        double mcc = -2.0;
        EvaluateWeightedEoC myEoC = new EvaluateWeightedEoC(
                false, cvModelPath, trnCvFold, tstCvFold, clsfNames, kFolds);
        try {
            mcc = myEoC.crossValidate(weightCmb, kFolds, trnData);
            eval = myEoC.getCVEvaluation();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        solution.setObjective(0, mcc);

    }

}
