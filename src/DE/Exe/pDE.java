/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DE.Exe;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmetal.core.*;
import jmetal.util.JMException;
import jmetal.util.comparators.ObjectiveComparator;
import jmetal.util.parallel.IParallelEvaluator;

/**
 * This class implements a differential evolution algorithm.
 */
public class pDE extends Algorithm {

    IParallelEvaluator parallelEvaluator_;
    private static String expPathName;

    /**
     * Constructor
     *
     * @param problem Problem to solve
     * @param evaluator Parallel evaluator
     */
    public pDE(Problem problem, IParallelEvaluator evaluator) {
        super(problem);

        parallelEvaluator_ = evaluator;
    } // pDE

    public pDE(Problem problem, IParallelEvaluator evaluator, String expName) {
        super(problem);
        this.expPathName = expName;
        parallelEvaluator_ = evaluator;
        System.out.println(expPathName);

    } // gDE

    /**
     * Runs the pDE algorithm.
     *
     * @return a <code>SolutionSet</code> that is a set of non dominated
     * solutions as a result of the algorithm execution
     * @throws jmetal.util.JMException
     */
    public SolutionSet execute() throws JMException, ClassNotFoundException {
        FileWriter fout = null;

        int populationSize;
        int maxEvaluations;
        int evaluations;

        SolutionSet population;
        SolutionSet offspringPopulation;

        Operator selectionOperator;
        Operator crossoverOperator;

        Comparator comparator;
        comparator = new ObjectiveComparator(0); // Single objective comparator

        // Differential evolution parameters
        int r1;
        int r2;
        int r3;
        int jrand;

        Solution parent[];

        //Read the parameters
        populationSize = ((Integer) this.getInputParameter("populationSize")).intValue();
        maxEvaluations = ((Integer) this.getInputParameter("maxEvaluations")).intValue();
        parallelEvaluator_.startEvaluator(problem_);

        selectionOperator = operators_.get("selection");
        crossoverOperator = operators_.get("crossover");

        //Initialize the variables
        population = new SolutionSet(populationSize);
        evaluations = 0;

        // Create the initial solutionSet
        /*Solution newSolution;
         for (int i = 0; i < populationSize; i++) {
         newSolution = new Solution(problem_);
         problem_.evaluate(newSolution);
         problem_.evaluateConstraints(newSolution);
         evaluations++;
         population.add(newSolution);
         //parallelEvaluator_.addSolutionForEvaluation(newSolution);

         } //for       */
        // Create the initial solutionSet
        Solution newSolution;
        for (int i = 0; i < populationSize; i++) {
            newSolution = new Solution(problem_);
            parallelEvaluator_.addSolutionForEvaluation(newSolution);
        }

        List<Solution> solutionList = parallelEvaluator_.parallelEvaluation();
        for (Solution solution : solutionList) {
            population.add(solution);
            evaluations++;
        }

        try {
            fout = new FileWriter( expPathName + "-Gen-BestIndiv.txt", true);
            fout.write("\n.....\nNew Run\n....\n");
            fout.write("Gen: " + evaluations + "\t" + Double.toString(population.get(populationSize - 1).getObjective(0)) + "\n");
            fout.close();

            // Generations ...
            population.sort(comparator);
            double bestFitness = -1.0;

            while (evaluations < maxEvaluations || bestFitness == 1.0) {

                for (int i = 0; i < populationSize; i++) {
                    // Obtain parents. Two parameters are required: the population and the 
                    //                 index of the current individual
                    parent = (Solution[]) selectionOperator.execute(new Object[]{population, i});
                    Solution child;

                    // Crossover. Two parameters are required: the current individual and the 
                    //            array of parents
                    child = (Solution) crossoverOperator.execute(new Object[]{population.get(i), parent});
                    parallelEvaluator_.addSolutionForEvaluation(child);
                    /*
                     problem_.evaluate(child);
                     evaluations++;

                     if (comparator.compare(population.get(i), child) < 0) {
                     offspringPopulation.add(new Solution(population.get(i)));
                     } else {
                     offspringPopulation.add(child);
                     }
                     */
                } // for

                offspringPopulation = new SolutionSet(populationSize);
                List<Solution> childSolutionList = parallelEvaluator_.parallelEvaluation();
                int pi = 0;
                for (Solution child : childSolutionList) {
                    if (comparator.compare(population.get(pi), child) > 0) {
                        offspringPopulation.add(new Solution(population.get(pi)));
                    } else {
                        offspringPopulation.add(child);
                    }
                    pi++;
                    evaluations++;
                }

                // The offspring population becomes the new current population
                population.clear();
                for (int i = 0; i < populationSize; i++) {
                    population.add(offspringPopulation.get(i));
                }
                offspringPopulation.clear();
                population.sort(comparator);
                bestFitness = population.get(populationSize - 1).getObjective(0);

                //Write Best Individual
                fout = new FileWriter( expPathName + "-Gen-BestIndiv.txt", true);
                fout.write("Gen: " + evaluations + "\t" + Double.toString(population.get(populationSize - 1).getObjective(0)) + "\n");
                fout.close();
            } // while

            parallelEvaluator_.stopEvaluator();

        } // execute
        catch (IOException ex) {
            Logger.getLogger(pDE.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fout.close();
            } catch (IOException ex) {
                Logger.getLogger(pDE.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // Return a population with the best individual
        SolutionSet resultPopulation = new SolutionSet(1);
        resultPopulation.add(population.get(population.size()-1));

        System.out.println("Evaluations: " + evaluations);
        return resultPopulation;
    } // execute
} // pDE
