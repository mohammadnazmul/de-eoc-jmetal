/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DE.Exe;

import ensemble.weighted.EvaluateWeightedEoC;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;
import problem.DEWeightedEOC;

import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.core.converters.ConverterUtils.DataSource;

/**
 *
 * @author mohammad
 */
public class DE_Main {
    
    // CIKM Paper : DE-HEoC
     public static String[] clsfNames = new String[]{
     "BayesNet", "DecisionStump", "DecisionTable", "IBk", "J48",
     "JRip", "KStar", "LibSVM", "Logistic", "LWL",
     "NaiveBayes", "OneR", "PART", "RandomTree", "REPTree",
     "SGD", "SimpleLogistic", "VFI", "VotedPerceptron", "ZeroR"
     };
    
/*
    public static String[] clsfNames = new String[]{
        "BayesNet", "NaiveBayes",
        "SMO", "SPegasos",
        "Logistic", "SimpleLogistic", "SGD",
        "MLPClassifier", "RBFNetwork", "VotedPerceptron",
        "ADTree", "BFTree", "HoeffdingTree", "J48", "LADTree", "REPTree", "PART",
        "DecisionStump",
        "ExtraTree", "FT", "LMT", "RandomTree", "SimpleCart",
        "IBk",
        "ConjunctiveRule", "JRip", "Ridor",
        "DecisionTable",
        "VFI"
    };
*/
    static Instances[] trn;
    static Instances[] tst;
    static String strTrnFile, strTstFile;
    static Instances dataTrn;
    static Instances dataTst;

    static int nTrials = 10;
    static int maxEvaluations = 1000;
    static int populationSize = 100;
    static double xoverRate = 0.6;
    static double factor = 0.9;
    static boolean isEvalOnly = false;
    static boolean isCvEnsemble = false;

    static String expName;
    static String solutionType = "Real";
    static int numberOfVariables = clsfNames.length;
    static int fold = 10;
    static String cvDPath; // = "/home/mohammad/test/AD-jMetal/CVData/";
    static String outPath;
    static String cvMPath, modelPath; // = "/home/mohammad/test/AD-jMetal/CVModel/";
    //strTrnFile = "/home/mohammad/test/AD-jMetal/Train-Data-AD-5-Prot.arff";

    public static void loadSettings(String fileName) {
        BufferedReader br = null;
        isEvalOnly = false;
        try {
            File fin = new File(fileName);
            br = new BufferedReader(new FileReader(fin));
            String line = null;
            String delims = "[ \t:]+";

            while ((line = br.readLine()) != null) {
                if (line.startsWith("expName:") == true) {
                    String[] tokens = line.split(delims);
                    expName = tokens[1];
                } else if (line.startsWith("modelPath:") == true) {
                    String[] tokens = line.split(delims);
                    modelPath = tokens[1];
                } else if (line.startsWith("trnFile:") == true) {
                    String[] tokens = line.split(delims);
                    strTrnFile = tokens[1];
                } else if (line.startsWith("tstFile:") == true) {
                    String[] tokens = line.split(delims);
                    strTstFile = tokens[1];
                } else if (line.startsWith("cvDataPath:") == true) {
                    String[] tokens = line.split(delims);
                    cvDPath = tokens[1];
                } else if (line.startsWith("cvModelPath:") == true) {
                    String[] tokens = line.split(delims);
                    cvMPath = tokens[1];
                    isCvEnsemble = true;
                } else if (line.startsWith("outPath:") == true) {
                    String[] tokens = line.split(delims);
                    outPath = tokens[1];
                } else if (line.startsWith("fold:") == true) {
                    String[] tokens = line.split(delims);
                    fold = Integer.parseInt(tokens[1]);
                } else if (line.startsWith("eval:") == true) {
                    isEvalOnly = true;
                } else if (line.startsWith("xover:") == true) {
                    String[] tokens = line.split(delims);
                    xoverRate = Double.parseDouble(tokens[1]);
                } else if (line.startsWith("f:") == true) {
                    String[] tokens = line.split(delims);
                    factor = Double.parseDouble(tokens[1]);
                } else if (line.startsWith("maxGen:") == true) {
                    String[] tokens = line.split(delims);
                    maxEvaluations = Integer.parseInt(tokens[1]);
                } else if (line.startsWith("popSize:") == true) {
                    String[] tokens = line.split(delims);
                    populationSize = Integer.parseInt(tokens[1]);
                } else if (line.startsWith("repeat:") == true) {
                    String[] tokens = line.split(delims);
                    nTrials = Integer.parseInt(tokens[1]);
                }
            }
            br.close();
        } catch (Exception ex) {
            Logger.getLogger(DE_Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    static void loadDatasets(String cvData, int nf) {
        try {
            trn = new Instances[nf];
            tst = new Instances[nf];

            ConverterUtils.DataSource source = new ConverterUtils.DataSource(strTrnFile);
            dataTrn = source.getDataSet();
            System.out.println("Training Set loaded: " + strTrnFile);
            dataTrn.setClassIndex(dataTrn.numAttributes() - 1);

            System.out.println("Loading CV data from : " + cvData);
            for (int i = 0; i < nf; i++) {
                source = new DataSource(cvData + "train-fold-" + i + ".arff");
                Instances train = source.getDataSet();
                train.setClassIndex(train.numAttributes() - 1);
                trn[i] = train;

                source = new ConverterUtils.DataSource(cvData + "test-fold-" + i + ".arff");
                Instances test = source.getDataSet();
                test.setClassIndex(test.numAttributes() - 1);
                tst[i] = test;
            }
        } catch (Exception ex) {
            Logger.getLogger(DE_Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main_old(String[] args) throws JMException, ClassNotFoundException, Exception {
        // output usage
        if (args.length == 0) {
            System.err.println("\nUsage: java DEEvol -i <input file containing the settings>\n");
            System.exit(1);
        } else if (args[0].equals("-i")) {
            String fname = args[1];
            loadSettings(fname);
        } else {
            System.err.println("\nUsage: java DEEvol -i <input file containing the settings>\n");
            System.exit(1);
        }

        // Execute the Differential Evolution Algorithm
        if (isEvalOnly == false) {
            loadDatasets(cvDPath, fold);
            runDEEoC();
        } else if (isCvEnsemble == true) {
            loadDatasets(cvDPath, fold);
            cvEnsemble();
        } else {
            evaluateEnsemble();
        }

    }

    static String printIndividual(List<Double> cmb) {
        String indiv = "\"[" + Double.toString(cmb.get(0));
        for (int i = 1; i < cmb.size(); i++) {
            indiv += ("," + Double.toString(cmb.get(i)));
        }
        indiv += "]\"";
        return indiv;
    }

    static void ReadResults(double[][] bestIndivs) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(expName + "-VAR"));
            String line;
            int rep = 0;
            while ((line = br.readLine()) != null) {
                // process the line.
                String[] tokens = line.split(" ");
                for (int i = 0; i < tokens.length; i++) {
                    bestIndivs[rep][i] = Double.parseDouble(tokens[i]);
                }
                rep++;
            }
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(DE_Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void evaluateEnsemble() throws Exception {
        double[][] bestIndivs = new double[nTrials][clsfNames.length];

        ReadResults(bestIndivs);

// Load Train & Test Data        
        ConverterUtils.DataSource source = new ConverterUtils.DataSource(strTrnFile);
        dataTrn = source.getDataSet();
        dataTrn.setClassIndex(dataTrn.numAttributes() - 1);

        source = new ConverterUtils.DataSource(strTstFile);
        dataTst = source.getDataSet();
        dataTst.setClassIndex(dataTst.numAttributes() - 1);

// evaluate each best individual  
        String txtResult = "Trial\tIndividual\tPrecision\tAccuracy\tF-Measure\tMCC\tTP\tTN\tFP\tFN\n";
        System.out.print(txtResult);
        for (int idx = 0; idx < nTrials; idx++) {
            try {
                List<Double> weightCmb = new ArrayList<Double>();
                for (int i = 0; i < clsfNames.length; i++) {
                    weightCmb.add(bestIndivs[idx][i]);
                }

//boolean dbg, String modelPath,
// Instances trainData, Instances testData, String[] classifierNames
                EvaluateWeightedEoC myEoC = new EvaluateWeightedEoC(
                        false, modelPath, dataTrn, dataTst, clsfNames);

                double mcc = myEoC.testModel(weightCmb);
                Evaluation eval = myEoC.getTestEvaluation();

                String resLine = idx + "\t";
                resLine += printIndividual(weightCmb) + "\t";
                resLine += eval.precision(0) + "\t" + eval.pctCorrect() + "\t" + eval.fMeasure(0) + "\t" + eval.weightedMatthewsCorrelation() + "\t";
                resLine += eval.numTruePositives(0) + "\t" + eval.numTrueNegatives(0) + "\t" + eval.numFalsePositives(0) + "\t" + eval.numFalseNegatives(0) + "\n";

                System.out.print(resLine);
                txtResult += resLine;
            } catch (Exception ex) {
                Logger.getLogger(DE_Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }// end For

        PrintWriter out = new PrintWriter(outPath + expName + "-EVAL");
        out.println(txtResult);
        out.close();
        System.out.println("\nDone!\n");
    }

    public static void cvEnsemble() throws Exception {
        double[][] bestIndivs = new double[nTrials][clsfNames.length];

        ReadResults(bestIndivs);


        String txtResult = "Trial\tIndividual\tPrecision\tAccuracy\tF-Measure\tMCC\tTP\tTN\tFP\tFN\n";
        System.out.print(txtResult);
        for (int idx = 0; idx < nTrials; idx++) {
            try {
                List<Double> weightCmb = new ArrayList<Double>();
                for (int i = 0; i < clsfNames.length; i++) {
                    weightCmb.add(bestIndivs[idx][i]);
                }

    // 1. loading cv models from path
    // public EvaluateWeightedEoC(boolean dbg,String cvModelPath,
    //        Instances[] cvTrnData, Instances[] cvTstData, String[] classifierNames, int fold)
                EvaluateWeightedEoC myEoC = new EvaluateWeightedEoC(
                        false, cvMPath, trn, tst, clsfNames, fold);
    
                double mcc = myEoC.crossValidate(weightCmb, fold, dataTrn);
                Evaluation eval = myEoC.getCVEvaluation();

                String resLine = idx + "\t";
                resLine += printIndividual(weightCmb) + "\t";
                resLine += eval.precision(0) + "\t" + eval.pctCorrect() + "\t" + eval.fMeasure(0) + "\t" + eval.weightedMatthewsCorrelation() + "\t";
                resLine += eval.numTruePositives(0) + "\t" + eval.numTrueNegatives(0) + "\t" + eval.numFalsePositives(0) + "\t" + eval.numFalseNegatives(0) + "\n";

                System.out.print(resLine);
                txtResult += resLine;
            } catch (Exception ex) {
                Logger.getLogger(DE_Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }// end For

        PrintWriter out = new PrintWriter(outPath + expName + "-CV-EVAL");
        out.println(txtResult);
        out.close();
        System.out.println("\nDone!\n");
    }

    public static void runDEEoC() throws JMException, ClassNotFoundException {
        //DEWeightedEOC(String solutionType, Integer numberOfVariables, int fold, String cvMpath, Instances[] trn, Instances[] tst, String[] clsf, Instances dataTrn) {
        Problem problem;         // The problem to solve
        Algorithm algorithm;         // The algorithm to use
        Operator crossover;         // Crossover operator
        Operator mutation;         // Mutation operator
        Operator selection;         // Selection operator

        HashMap parameters; // Operator parameters

        problem = new DEWeightedEOC(solutionType, numberOfVariables, fold, cvMPath, trn, tst, clsfNames, dataTrn);
        algorithm = new DE(problem, expName);   // Asynchronous cGA

        /* Algorithm parameters*/
        algorithm.setInputParameter("populationSize", populationSize);
        algorithm.setInputParameter("maxEvaluations", maxEvaluations);

        // Crossover operator 
        parameters = new HashMap();
        parameters.put("CR", xoverRate);
        parameters.put("F", factor);
        parameters.put("DE_VARIANT", "rand/1/bin");

        crossover = CrossoverFactory.getCrossoverOperator("DifferentialEvolutionCrossover", parameters);

        // Add the operators to the algorithm
        parameters = null;
        selection = SelectionFactory.getSelectionOperator("DifferentialEvolutionSelection", parameters);

        algorithm.addOperator("crossover", crossover);
        algorithm.addOperator("selection", selection);

        SolutionSet allSoln = new SolutionSet(nTrials);
        for (int rep = 0; rep < nTrials; rep++) {
            /* Execute the Algorithm */
            System.out.print("Run " + (rep + 1) + ":\t");
            long initTime = System.currentTimeMillis();
            SolutionSet population = algorithm.execute();
            long estimatedTime = System.currentTimeMillis() - initTime;
            allSoln.add(population.get(0));
            System.out.print("\tMCC: " + population.get(0).getObjective(0));
            System.out.println("\tExecution time: " + estimatedTime);
        }

        /* Log messages */
        System.out.println("Objectives values have been writen to file FUN");
        allSoln.printObjectivesToFile(outPath + expName + "-FUN");
        System.out.println("Variables values have been writen to file VAR");
        allSoln.printVariablesToFile(outPath + expName + "-VAR");

    }
} // DE_main
