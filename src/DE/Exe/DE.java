//  DE.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
package DE.Exe;

import static DE.Exe.DE_Main.outPath;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmetal.core.*;
import jmetal.util.JMException;
import jmetal.util.comparators.ObjectiveComparator;

/**
 * This class implements a differential evolution algorithm.
 */
public class DE extends Algorithm {

    private static String expName;

    /**
     * Constructor
     *
     * @param problem Problem to solve
     */
    public DE(Problem problem) {
        super(problem);
    } // gDE

    public DE(Problem problem, String expName) {
        super(problem);
        this.expName = expName;
    } // gDE

    /**
     * Runs of the DE algorithm.
     *
     * @return a <code>SolutionSet</code> that is a set of non dominated
     * solutions as a result of the algorithm execution
     * @throws JMException
     */
    public SolutionSet execute() throws JMException, ClassNotFoundException {
        FileWriter fout = null;

        int populationSize;
        int maxEvaluations;
        int evaluations;
        SolutionSet population;
        SolutionSet offspringPopulation;
        Operator selectionOperator;
        Operator crossoverOperator;
        Comparator comparator;
        comparator = new ObjectiveComparator(0); // Single objective comparator
        // Differential evolution parameters
        int r1;
        int r2;
        int r3;
        int jrand;
        Solution parent[];
        //Read the parameters
        populationSize = ((Integer) this.getInputParameter("populationSize")).intValue();
        maxEvaluations = ((Integer) this.getInputParameter("maxEvaluations")).intValue();
        selectionOperator = operators_.get("selection");
        crossoverOperator = operators_.get("crossover");
        //Initialize the variables
        population = new SolutionSet(populationSize);
        evaluations = 0;
        // Create the initial solutionSet
        Solution newSolution;
        for (int i = 0; i < populationSize; i++) {
            newSolution = new Solution(problem_);
            problem_.evaluate(newSolution);
            //problem_.evaluateConstraints(newSolution);
            evaluations++;
            population.add(newSolution);
        } //for   

        try {
            fout = new FileWriter(outPath + expName + "-Gen-BestIndiv.txt", true);
            fout.write("\n.....\nNew Run\n....\n");
            fout.write("Gen: " + evaluations + "\t" + Double.toString(population.get(populationSize - 1).getObjective(0)) + "\n");
            fout.close();
            // Generations ...            population.sort(comparator);
            double bestFitness = -1.0;
            while (evaluations < maxEvaluations || bestFitness == 1.0) {

                // Create the offSpring solutionSet
                offspringPopulation = new SolutionSet(populationSize);
                //offspringPopulation.add(new Solution(population.get(0))) ;
                for (int i = 0; i < populationSize; i++) {
                    // Obtain parents. Two parameters are required: the population and the
                    //                 index of the current individual
                    parent = (Solution[]) selectionOperator.execute(new Object[]{population, i});

                    Solution child;

                    // Crossover. Two parameters are required: the current individual and the
                    //            array of parents
                    child = (Solution) crossoverOperator.execute(new Object[]{population.get(i), parent});

                    problem_.evaluate(child);

                    evaluations++;

                   // System.out.println(population.get(i).getObjective(0)+"\t"+child.getObjective(0));
                    if (comparator.compare(population.get(i), child) > 0) {
                        offspringPopulation.add(new Solution(population.get(i)));
                    } else {
                        offspringPopulation.add(child);
                    }
                } // for
                // The offspring population becomes the new current population
                population.clear();
                for (int i = 0; i < populationSize; i++) {
                    population.add(offspringPopulation.get(i));
                }
                offspringPopulation.clear();
                population.sort(comparator);
                bestFitness = population.get(populationSize - 1).getObjective(0);

                //Write Best Individual
                fout = new FileWriter(outPath+expName + "-Gen-BestIndiv.txt", true);
                fout.write("Gen: " + evaluations + "\t" + Double.toString(population.get(populationSize - 1).getObjective(0)) + "\n");
                fout.close();
            } // while
            //System.out.println("Evaluations: " + evaluations);

        } // execute
        catch (IOException ex) {
            Logger.getLogger(DE.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fout.close();
            } catch (IOException ex) {
                Logger.getLogger(DE.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // Return a population with the best individual
        SolutionSet resultPopulation = new SolutionSet(1);
        resultPopulation.add(population.get(populationSize - 1));
        return resultPopulation;
    }
} // DE
